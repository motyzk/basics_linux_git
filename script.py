from glob import glob

def read_files():
    files = [f for f in glob("**/*.txt", recursive=True) if f != "contents.txt"]

    final_text = []

    for f in files:
        with open(f, 'r') as text:
            final_text += [text.read()]

    final_text_sorted = sorted(final_text)
    final_text_sorted_as_a_string = "\n".join(final_text_sorted)

    with open("contents.txt", 'w') as output:
        output.write(final_text_sorted_as_a_string)
    
    return final_text_sorted_as_a_string


print(read_files())