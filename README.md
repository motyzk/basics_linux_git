# list of commands:
___

Command                        | Description
------------------------------ | ------------------------------------------
mkdir Basics                   | Creating a directory for this assignment
cd Basics                      | Changing directory to basics
git init                       | Initalizing the git repo
touch script.py                | Creating the python file
code .                         | Opening this folder in my VS code
python3 script.py              | Run the file
touch text1.txt                | Creating dummy text files
git log                        | Basic details about the repo
git add .                      | Adding all the files from to the staging area
git commit -m "initial commit" | First commit
git log                        | Seeing the list of commits
git remote add origin <url>    | adding the remote
git push origin master         | pushing the changes to the remote repo 